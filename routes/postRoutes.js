const express = require("express");

const router = express.Router();

const { postForm, storePost, posts, details, update, postValidations, postUpdate, postDelete } = require("../controllers/postsController");
const { auth } = require("../middlewares/auth");

router.get("/add_post", postForm);
router.post("/save_post", auth, storePost);
router.get("/posts/:page", auth, posts);
router.get("/post_details/:id", auth, details);
router.get("/post_update/:id", auth, update);
router.post("/update_post", [postValidations, auth], postUpdate);
router.get("/post_delete/:id", auth, postDelete);
module.exports = router;