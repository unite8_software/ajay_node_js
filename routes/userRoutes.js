const express = require("express");
const router = express.Router();

// User Controller
const { loadSignUp, loadLogin, registerValidations, userFormRegister, userLogin, loginValidations } = require('../controllers/userController');
const { stopLogin } = require("../middlewares/auth");

router.get("/", stopLogin, loadSignUp);
router.get("/login", stopLogin, loadLogin);
router.post("/userRegister", registerValidations, userFormRegister);
router.post("/userLogin", loginValidations, userLogin);

module.exports = router;