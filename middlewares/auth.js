const jwt = require("jsonwebtoken");

const auth = (req, res, next) => {
    res.setHeader('Content-Type', 'text/html');
    if (req.session.user) {
        const token = req.session.user;
        const verified = jwt.verify(token, process.env.JWT_SECRET);
        if (verified) {
            req.id = verified.userID;
        } else {
            res.redirect("/login");
        }
    } else {
        res.redirect("/login");
    }
    next();
}

const stopLogin = (req, res, next) => {
    res.setHeader('Content-Type', 'text/html');
    if (req.session.user) {
        const token = req.session.user;
        const verified = jwt.verify(token, process.env.JWT_SECRET);
        if (verified) {
            res.redirect("/profile");
        }
    }
    next();
}

module.exports = {
    auth,
    stopLogin
}