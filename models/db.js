const mongoose = require("mongoose");

const connect = async () => {
    try {
        mongoose.connect(process.env.DB, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log("DB connected")
    } catch (err) {
        console.log(err.message);
    }
}

module.exports = connect;