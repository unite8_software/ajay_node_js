const mongoose = require('mongoose')

// Connect mongodb
const connection = async () => {
    try {
        await mongoose.connect("mongodb://127.0.0.1:27017/nodeabcblog", { useNewUrlParser: true, useUnifiedTopology: true })
        console.log("Mongodb Connected");
    } catch (err) {
        console.log(err.message);
    }
}

connection();

//Define Schema
const userSchema = new mongoose.Schema({
    name: { type: String, required: true, minLength: 3 }, email: { type: String, required: true }, phone: { type: Number, required: true }
}, { timestamps: true });

//Create Model
const Users = mongoose.model("users", userSchema);

// Insert data

// const name = "Ajay Srivastava";
// const email = "abc@gmail.com";
// const phone = 7355997730;
// const newUser = new Users({
//     name, email, phone
// });

// newUser.save().then(user => {
//     console.log(user)
// }).catch(err => {
//     console.log(err.message)
// });


// Fetch all

// Users.find().then(users => {
//     console.log(users);
// }).catch(error => {
//     console.log(error.message);
// });

// Fetch Specific

// Users.find({email: "ajay@gmail.com"}).then(users => {
//     console.log(users);
// }).catch(error => {
//     console.log(error.message);
// });

//Find by id and update

// Users.findByIdAndUpdate("604210a65d21830bec8646c9", {email: "ajay@gmail.com"}).then(user => {
//     console.log(user);
// }).catch(error => {
//     console.log(error.message)
// });

//Find by id and delete

// Users.findByIdAndDelete("604210a65d21830bec8646c9").then(() => {
//     console.log("document deleted successfully");
// }).catch(error => {
//     console.log(error.message);
// })