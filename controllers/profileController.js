const Users = require('../models/User');

const profile = async (req, res) => {
    const id = req.id;
    const user = await Users.findOne({ _id: id });
    res.render("profile", { title: "Profile", login: true, user });
};

const postForm = (req, res) => {
    res.render('add_post', { title: 'Create new post', login: true })
}

const logout = (req, res) => {
    req.session.destroy(err => {
        if (!err) {
            res.redirect('login');
        }
    });
}

module.exports = {
    profile,
    postForm,
    logout
} 