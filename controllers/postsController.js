const { check, validationResult } = require("express-validator");
const formidable = require('formidable');
const { v4: uuidv4 } = require('uuid');
const fs = require("fs");
const dateFormat = require("dateformat");

const Users = require("../models/User");
const Posts = require("../models/Posts");

const postForm = (req, res) => {
    res.render('add_post', { title: 'Create new post', login: true, errors: [], post_title: '', post_body: '' })
}

const storePost = (req, res) => {
    const form = formidable({ multiples: true });
    form.parse(req, (err, fields, files) => {
        const errors = [];
        const { post_title, post_body } = fields;
        if (post_title.length === 0) {
            errors.push({ 'msg': 'Title is required' });
        }
        if (post_body.length === 0) {
            errors.push({ 'msg': 'Body is required' });
        }
        if (files.post_image.name === 0) {
            errors.push({ 'msg': 'Image is required' });
        }

        const imageName = files.post_image.name;
        const split = imageName.split(".");
        const imgExt = split[split.length - 1].toUpperCase();
        if (files.post_image.name.length === 0) {
            errors.push({ 'msg': 'Image is required' });
        } else if (imgExt !== "JPG" && imgExt !== "PNG") {
            errors.push({ 'msg': 'Only jpg and png are allowed' });
        }

        console.log(errors);
        if (errors.length !== 0) {
            res.render("add_post", { 'title': 'Create new post', 'login': true, errors, post_title, post_body });
        } else {
            files.post_image.name = uuidv4() + "." + imgExt;
            const oldPath = files.post_image.path;
            const newPath = __dirname + "/../views/assets/img/" + files.post_image.name;
            fs.readFile(oldPath, (err, data) => {
                if (!err) {
                    fs.writeFile(newPath, data, err => {
                        fs.unlink(oldPath, async (err) => {
                            if (!err) {
                                // res.send("Image upload");
                                const id = req.id;
                                try {
                                    const user = await Users.findOne({ _id: id });
                                    const name = user.name;
                                    const newPost = new Posts({
                                        userID: id, title: post_title, body: post_body, image: files.post_image.name, userName: name
                                    });
                                    try {
                                        const result = await newPost.save();
                                        if (result) {
                                            req.flash("success", "Your post has been added");
                                            res.redirect("/posts/1");
                                        }
                                    } catch (err) {
                                        res.send(err.msg);
                                    }
                                } catch (err) {
                                    res.send(err.msg);
                                }
                            }
                        })
                    })
                }
            });
        }
    });
}

const posts = async (req, res) => {
    const id = req.id;
    let currentPage = 1;
    let page = req.params.page;
    if (page) {
        currentPage = page;
    }
    const perPage = 3;
    const skip = (currentPage - 1) * perPage;
    try {
        const allPosts = await Posts.find({ userID: id }).skip(skip).limit(perPage).sort({ updatedAt: -1 });
        const count = await Posts.find({ userID: id }).countDocuments();
        res.render("allposts", { title: 'Posts', login: true, posts: allPosts, formate: dateFormat, count, perPage, currentPage, });
    } catch (err) {
        res.send(err);
    }
}

const details = async (req, res) => {
    const id = req.params.id;
    try {
        const details = await Posts.findOne({ _id: id });
        res.render("post_details", { title: 'Post details', login: true, details })
    } catch (err) {
        res.send(err);
    }
}

const update = async (req, res) => {
    const id = req.params.id;
    try {
        const post = await Posts.findOne({ _id: id });
        res.render("update_post", { title: 'Post update', login: true, post, errors: [] })
    } catch (err) {
        res.send(err);
    }
}

const postValidations = [
    check('post_title').not().isEmpty().withMessage('Title is required'),
    check('post_body').not().isEmpty().withMessage('Body is required')
];

const postUpdate = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const post_id = req.body.post_id;
        const post = await Posts.findOne({ _id: post_id });
        res.render("update_post", { title: 'Update Post', login: true, errors: errors.array(), post });
    } else {
        const { post_id, post_title, post_body } = req.body;
        try {
            const updatePost = await Posts.findByIdAndUpdate(post_id, { title: post_title, body: post_body });
            if (updatePost) {
                req.flash("success", "Your past has been updated successfully");
                res.redirect('/posts/1');
            } else {
                res.send("post not coming");
            }
        } catch (err) {
            res.send(err);
        }
    }
}

const postDelete = async(req, res) => {
    const id = req.params.id;
    try {
        const deleted = await Posts.findByIdAndRemove(id);
        if (deleted) {
            req.flash("success", "Your past has been deleted successfully");
            res.redirect('/posts/1');
        }
    } catch (err) {
        res.send(err);
    }
}

module.exports = {
    postForm, storePost, posts, details, update, postValidations, postUpdate, postDelete
} 