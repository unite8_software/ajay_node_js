const { check, validationResult } = require("express-validator");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Users = require("../models/User");

const loadSignUp = (req, res) => {
    const title = "Create new account";
    const errors = [];
    res.render("register", { title, errors, inputs: {}, login: false });
};

const registerValidations = [
    check("name").isLength({ min: 3 }).withMessage("Name is required & must be 3 characters long"),
    check("email").isEmail().withMessage("Valid email is required"),
    check("password").isLength({ min: 6 }).withMessage("Password must be 6 characters long"),
];

const userFormRegister = async (req, res) => {
    const { name, email, password } = req.body;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        title = "Create new account";
        res.render("register", { title, errors: errors.array(), inputs: req.body, login: false });
    } else {
        try {
            const userEmail = await Users.findOne({ email });
            if (userEmail) {
                res.render("register", { title: "Create new account", errors: [{ msg: 'Email id already registered' }], inputs: req.body });
            } else {
                const salt = await bcrypt.genSalt(10);
                const hashed = await bcrypt.hash(password, salt);
                const newUser = new Users({
                    name: name, email: email, password: hashed
                });
                try {
                    const createdUser = await newUser.save();
                    if (createdUser) {
                        req.flash("success", "Your account has been created successfully");
                        res.redirect("/login");
                    }
                } catch (err) {
                    console.log(err)
                }
            }
        } catch (err) {
            console.log(err)
        }
    }
}

const loadLogin = (req, res) => {
    title = "User login";
    res.render("login", { title, errors: [], inputs: {}, login: false });
}

const loginValidations = [
    check('email').not().isEmpty().isEmail().withMessage('Valid email is required'),
    check('password').not().isEmpty().withMessage('Password is required')
];

const userLogin = async (req, res) => {
    const { email, password } = req.body;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.render("login", { title: 'User login', errors: errors.array(), inputs: req.body, login: false });
    } else {
        const checkEmail = await Users.findOne({ email });
        if (!checkEmail) {
            res.render("login", { title: 'User login', errors: [{ 'msg': 'Email not registered' }], inputs: req.body, login: false });
        } else {
            const id = checkEmail._id;
            const pswdVerify = await bcrypt.compare(password, checkEmail.password);
            if (pswdVerify) {
                // Create Token
                const token = jwt.sign({ userID: id }, process.env.JWT_SECRET, { expiresIn: '7d' });
                // Create Session
                req.session.user = token;
                res.redirect("/profile");
            } else {
                res.render("login", { title: 'User login', errors: [{ 'msg': 'Password do not match' }], inputs: req.body, login: false });
            }
        }
    }
}

module.exports = {
    loadSignUp,
    loadLogin,
    registerValidations,
    userFormRegister,
    loginValidations,
    userLogin
}